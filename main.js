var accordions = document.getElementsByClassName('accordion');
// console.log(accordions);
for (var i=0; i < accordions.length; i++){
    accordions[i].onclick = function() {
        this.classList.toggle('is-open');

        var content = this.nextElementSibling;
        if (content.style.maxHeight){
            // accordion is ouvert
            content.style.maxHeight = null;
        } else {
            // accordion is fermer
            content.style.maxHeight = content.scrollHeight + 'px';
        };
    };
};